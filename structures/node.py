class TreeNode:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __repr__(self):
        return """<TreeNode val={0}, left={1}, right={2} >""".format(self.val, self.left, self.right)

    def print_inorder(self):
        if self.left:
            self.left.print_inorder()
        print(self.val)
        if self.right:
            self.right.print_inorder()
        return

    def print_preorder(self):
        if self.left:
            self.left.print_inorder()
        if self.right:
            self.right.print_inorder()
        print(self.val)
        return

    def print_postorder(self):
        print(self.val)
        if self.left:
            self.left.print_inorder()
        if self.right:
            self.right.print_inorder()
        return

class LinkedListNode:
    def __init__(self, val, next=None):
        self.val = val
        self.next = next

    def __repr__(self):
        return "<LinkedListNode val={0}, next={1}>".format(self.val, True if self.next else False)

    def set_next(self, next):
        self.next = next

