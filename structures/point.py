class MazePoint():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, point):
        if self.x == point.x and self.y == point.y:
            return True
        return False

    def north(self):
        return MazePoint(self.x - 1, self.y)

    def south(self):
        return MazePoint(self.x + 1, self.y)

    def east(self):
        return MazePoint(self.x, self.y + 1)

    def west(self):
        return MazePoint(self.x, self.y - 1)

    def __repr__(self):
        return "<MazePoint x={0}, y={1}>".format(self.x, self.y)
