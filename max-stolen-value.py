"""
Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob given the constraint. For example, if the list was [1,2,3,1], the max you can rob is 4 (house at index 0 and 2). If the list was [10, 1, 899, 1000], the max you can rob is 1010 (house at index 0 and 3).
"""

def max_stolen_value(houses):
    if not houses:
        return 0
    if len(houses) == 1:
        return houses[0]

    best_houses = [houses[0], max(houses[0], houses[1])]
    idx = 2
    while idx < len(houses):
        current_best = max(houses[idx] + best_houses[idx - 2], best_houses[idx - 1])
        best_houses.append(current_best)
        idx += 1
    return best_houses[-1]

houses = [1, 2, 3, 1]
print("{} => {}".format(houses, max_stolen_value(houses)))
houses = [10, 1, 899, 1000]
print("{} => {}".format(houses, max_stolen_value(houses)))
