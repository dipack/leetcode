from structures import LinkedListNode

def detect_loop(head: LinkedListNode, debug=True) -> LinkedListNode:
    """
    Detect at what node does the cycle in a Linked List start
    Algo:
    1. Move fastr at 2 jumps per, and slowr at 1 jump per
    2. If they intersect, they're in a loop, else there is no loop
    3. In case they intersect, restart slowr from the beginning,
        keeping fastr where it is, and jump both pointer 1 jump at a time
    4. Their intersection is the loop start
    """
    slowr, fastr = head, head
    while fastr is not None and fastr.next is not None:
        if debug:
            print("fastr at node {}".format(fastr))
            print("slowr at node {}".format(slowr))
        slowr = slowr.next
        fastr = fastr.next.next
        if fastr == slowr:
            break

    if slowr is None or fastr is None:
        # No loop
        if debug:
            print("No loop found!")
        return None

    if debug:
        print("Setting slowr to HEAD")
    slowr = head
    while slowr != fastr:
        if debug:
            print("fastr at node {}".format(fastr))
            print("slowr at node {}".format(slowr))
        slowr = slowr.next
        fastr = fastr.next

    return fastr

if __name__ == "__main__":
    a = LinkedListNode(0)
    b = LinkedListNode(1)
    c = LinkedListNode(2)
    d = LinkedListNode(3)
    e = LinkedListNode(4)
    f = LinkedListNode(5)
    a.set_next(b)
    b.set_next(c)
    c.set_next(d)
    d.set_next(e)
    e.set_next(f)
    f.set_next(b)
    print(detect_loop(a))

