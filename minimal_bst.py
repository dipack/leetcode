from structures import TreeNode

def create_min_bst(arr, start, end):
    """
    Given a sorted array, of unique ints,
    create a BST of min height
    Algo:
        Use the merge sort algo of divide and conquer, i.e.,
        mid of the array should be the root, left and right elements
        must be divided and inserted the same way
    """
    if start > end:
        return None

    mid = (start + end) // 2
    root = TreeNode(arr[mid])
    root.left = create_min_bst(arr, start, mid - 1)
    root.right = create_min_bst(arr, mid + 1, end)
    return root

if __name__ == "__main__":
    arr = [0, 1, 2, 3, 4, 5, 6, 7]
    tree = create_min_bst(arr, 0, len(arr) - 1)
    print(tree)

