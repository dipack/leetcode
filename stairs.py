"""
Count ways to reach the nth stair
https://www.geeksforgeeks.org/count-ways-reach-nth-stair/
https://www.geeksforgeeks.org/count-ways-reach-nth-stair-using-step-1-2-3/
"""

def count12(num):
    """
    If you can only jump 1, or 2 stairs at a time to reach the nth step
    """
    jumps = [0] * (num + 1)
    jumps[0:3] = [1, 1, 2]
    for num in range(3, num + 1):
        jumps[num] = jumps[num - 2] + jumps[num - 1]
    return jumps[num]

def count123(num):
    """
    If you can only jump 1, 2, or 3 stairs at a time to reach the nth step
    """
    jumps = [0] * (num + 1)
    jumps[0:3] = [1, 1, 2]
    for num in range(3, num + 1):
        jumps[num] = jumps[num - 3] + jumps[num - 2] + jumps[num - 1]
    return jumps[num]

if __name__ == "__main__":
    for num in [5, 10, 15, 100]:
        print("Number of ways to reach stair number {} using only 1/2 stair jumps at a time is {}".format(num, count12(num)))
        print("Number of ways to reach stair number {} using only 1/2/3 stair jumps at a time is {}".format(num, count123(num)))
