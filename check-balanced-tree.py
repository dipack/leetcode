import sys
from structures import TreeNode

# Equivalent to Integer.MIN_VALUE
UNBALANCED = -(sys.maxsize) - 1

def check_height(root):
    """
    Check if the given binary tree is balanced, i.e.,
    the difference of heights between the left and right
    subtrees is not greater than 1
    """
    if root is None:
        return 0

    lheight = check_height(root.left)
    if lheight == UNBALANCED:
        return UNBALANCED
    rheight = check_height(root.right)
    if rheight == UNBALANCED:
        return UNBALANCED

    if abs(lheight - rheight) > 1:
        return UNBALANCED
    return max(lheight, rheight) + 1

def is_balanced(root):
    return check_height(root) != UNBALANCED

if __name__ == "__main__":
    tree = TreeNode(0,
            TreeNode(1,
                TreeNode(2),
                TreeNode(3,
                    TreeNode(4),
                    None)
                ),
            TreeNode(5,
                TreeNode(6,
                    TreeNode(7,
                        TreeNode(8),
                        None),
                    TreeNode(9)
                    ),
                None)
            )
    print(tree)
    print("Is the tree balanced? {}".format(is_balanced(tree)))
    tree = TreeNode(0,
            TreeNode(1),
            TreeNode(2)
            )
    print(tree)
    print("Is the tree balanced? {}".format(is_balanced(tree)))
