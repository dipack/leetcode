from structures import TreeNode

def dfs(graph, start, visited=None, debug=True):
    if visited is None:
        visited = set()
    if debug:
        print("Visting vertex {}".format(start))
    visited.add(start)
    for node in graph[start]:
        if node not in visited:
            dfs(graph, node, visited)
    return visited

def dfs_tree(root, visited=None, debug=True):
    if visited is None:
        visited = set()
    if root is None:
        return visited
    if debug:
        print("Visiting vertex {}".format(root))
    visited.add(root)
    dfs_tree(root.left, visited, debug)
    dfs_tree(root.right, visited, debug)
    return visited

if __name__ == "__main__":
    graph = {'a': set(['b', 'c']), 'b': set(['a', 'd', 'e']), 'c': set(['a', 'f']), 'd': set(['b']), 'e': set(['b', 'f']), 'f': set(['c', 'e'])}
    print(dfs(graph, 'c', None))
    tree = TreeNode(1,
            TreeNode(2,
                TreeNode(3),
                TreeNode(4,
                    None, TreeNode(5))),
                TreeNode(6,
                    TreeNode(7), None))
    print(dfs_tree(tree))
