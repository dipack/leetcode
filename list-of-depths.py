from structures import TreeNode

def create_list_of_nodes(root):
    """
    Given a tree,
    return the list of nodes at each level
    Algo:
        Follow BFS
    """
    list_of_nodes = []
    current_level_nodes = []
    if root is not None:
        current_level_nodes.append(root)
    while len(current_level_nodes) > 0:
        list_of_nodes.append(current_level_nodes)
        leaves = current_level_nodes
        current_level_nodes = []
        for leaf in leaves:
            if leaf.left:
                current_level_nodes.append(leaf.left)
            if leaf.right:
                current_level_nodes.append(leaf.right)
    return list_of_nodes

if __name__ == "__main__":
    tree = TreeNode(0,
            TreeNode(1,
                TreeNode(2),
                TreeNode(3,
                    TreeNode(4),
                    None)
                ),
            TreeNode(5,
                TreeNode(6,
                    TreeNode(7,
                        TreeNode(8),
                        None),
                    TreeNode(9)
                    ),
                None)
            )

    nodes = create_list_of_nodes(tree)
    for lnodes in nodes:
        print(len(lnodes))
    print(nodes)

