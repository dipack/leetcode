mmap = {}

def fib(num):
    """
    Print the nth fibonacci number.
    Uses memoization to accelerate execution.
    """
    if num in mmap:
        return mmap[num]
    if num == 0:
        return 0
    if num == 1:
        return 1
    mmap[num] = fib(num - 2) + fib(num - 1)
    return mmap[num]

if __name__ == "__main__":
    for num in [3, 10, 50, 100, 500, 1000]:
        print("Number {} in Fibonacci sequence is {}".format(num, fib(num)))

