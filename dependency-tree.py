def resolve(deps, package):
    """
    Display all packages that need to be built,
    before the target package can be built.
    """
    seen = set()
    current = deps[package]
    while current:
        pack = current.pop(0)
        if pack not in seen:
            seen.add(pack)
            if pack in deps:
                current.extend(deps[pack])
    return list(seen)

if __name__ == "__main__":
    deps = {"a": ["b", "c"], "c": ["d"], "d": ["e", "b"]}
    package = "a"
    print("Package {}, Dependencies that need to be built {}".format(package, resolve(deps, package)))

