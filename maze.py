from structures import MazePoint

def is_point_valid(maze, point):
    return 0 <= point.y < len(maze) and 0 <= point.x < len(maze[0])

def dfs(maze, visited, point, end, debug=True):
    if not is_point_valid(maze, point):
        if debug:
            print("Invalid point {}".format(point))
        return False
    if visited[point.x][point.y]:
        if debug:
            print("Point visited {}".format(point))
        return False
    if point == end:
        visited[point.x][point.y] = True
        if debug:
            print("Reached the finish point")
        return True
    if maze[point.x][point.y] == 0:
        if debug:
            print("Wall")
        return False
    visited[point.x][point.y] = True
    if dfs(maze, visited, point.north(), end, debug):
        return True
    if dfs(maze, visited, point.east(), end, debug):
        return True
    if dfs(maze, visited, point.south(), end, debug):
        return True
    if dfs(maze, visited, point.west(), end, debug):
        return True
    visited[point.x][point.y] = False
    return False

def bfs(maze, visited, point, end, debug=True):
    visited[point.x][point.y] = True
    queue = [point]
    while queue:
        node = queue.pop(0)
        if debug:
            print("Visiting node {}".format(node))
        if node == end:
            if debug:
                print("Reached the finish point")
            return True
        nnodes = [node.north(), node.east(), node.south(), node.west()]
        for adj_node in nnodes:
            if is_point_valid(maze, adj_node) and not visited[adj_node.x][adj_node.y] and maze[adj_node.x][adj_node.y] == 1:
                visited[adj_node.x][adj_node.y] = True
                queue.append(adj_node)
    return False

def setup():
    maze = [[1, 0, 0, 0, 0, 0],
            [1, 1, 1, 1, 1, 1],
            [1, 0, 1, 0, 0, 0],
            [1, 0, 1, 1, 1, 0],
            [1, 1, 1, 1, 1, 1],
            [0, 0, 0, 0, 1, 0]]
    rows, columns = len(maze), len(maze[0])
    visited = [[False for _ in range(columns)] for _ in range(rows)]
    start_point = MazePoint(0, 0)
    end_point = MazePoint(5, 4)
    return maze, visited, start_point, end_point

def start():
    MAZE, visited, start_point, end_point = setup()
    if dfs(MAZE, visited, start_point, end_point):
        print("Solved using DFS")
    MAZE, visited, start_point, end_point = setup()
    if bfs(MAZE, visited, start_point, end_point):
        print("Solved using BFS")
    return

if __name__ == "__main__":
    start()
