import math

SPACE = " "
class Line:
    def __init__(self, limit):
        self.limit = limit
        self.current_len = 0
        self.words = []
        self.spaces = []
        self.is_last_line = False

    def can_add_word(self, word):
        if len(word) + self.current_len > self.limit:
            return False
        return True

    def add_word(self, word):
        self.words.append(word)
        self.current_len += len(word)
        return

    def set_last_line(self, is_last_line):
        self.is_last_line = is_last_line
        return

    def fill_spaces(self):
        if self.is_last_line:
            return self.fill_spaces_end()
        else:
            return self.fill_spaces_between()

    def add_single_space(self):
        self.spaces.append(SPACE)
        self.current_len += 1
        return

    def fill_spaces_end(self):
        if not self.spaces:
            self.spaces.append("")
        rem = self.limit - self.current_len
        self.spaces[-1] += "".join([SPACE] * rem)
        self.current_len += rem
        return

    def fill_spaces_between(self):
        remaining_space = self.limit - self.current_len
        if not self.spaces:
            self.spaces.append("")
        i = 0
        while remaining_space > 0:
            self.spaces[i] += SPACE
            self.current_len += 1
            remaining_space -= 1
            i = i + 1 if i + 1 < len(self.spaces) else 0
        return

    def to_str(self):
        s = ""
        idx = 0
        for word in self.words:
            s += word
            if idx < len(self.spaces):
                s += self.spaces[idx]
                idx += 1
        return s

def full_justify(words, max_width):
    justified = []
    if not words:
        return "".join(justified)

    lines = []
    curr_line = Line(max_width)
    idx = 0
    while idx < len(words):
        word = words[idx]
        if idx == len(words) - 1:
            curr_line.set_last_line(True)
        if curr_line.can_add_word(word):
            curr_line.add_word(word)
            if idx + 1 < len(words) and curr_line.can_add_word(words[idx + 1] + SPACE):
                curr_line.add_single_space()
            else:
                curr_line.fill_spaces()
            idx += 1
        else:
            curr_line.fill_spaces()
            lines.append(curr_line)
            curr_line = Line(max_width)

    curr_line.fill_spaces()
    lines.append(curr_line)

    return [line.to_str() for line in lines]

if __name__ == "__main__":
    words = ["This", "is", "an", "example", "of", "text", "justification."]
    max_width = 16
    print(full_justify(words, max_width))


