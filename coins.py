"""
Print the number of coins, and the denominations, required to make up a
certain amount in cents.
"""
def num_coins(cents):
    coins = [25, 10, 5, 1]
    res = []
    for c in coins:
        if c > cents:
            continue
        while cents >= c:
            cents -= c
            res.append(c)
            if cents == 0:
                break
    return res

for cents in [33, 31, 24, 0]:
    print("Cents: {}, Coins: {}".format(cents, num_coins(cents)))

