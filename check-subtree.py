from structures import TreeNode

"""
Given two trees, find if one tree is a subtree of the other.
Algo:
    Two ways:
    1. If root of t2 is found in t1, recursively compare both trees
    2. Else, compare the string reps of the pre-order traversal of
        both trees, and check if t2 is a substring of t1. Why pre-order?
        Two BSTs will have the same in-order traversals. Pre-order avoids that.
        E.g.: TN(4, None, TN(3)) and TN(4, TN(3), None) have the same pre-order
        traversal if we don't show the None nodes.
"""

def contains_tree(t1, t2):
    if t2 is None:
        return True
    return subtree_of(t1, t2)

def subtree_of(t1, t2):
    if t1 is None:
        return False
    if t1.val == t2.val and match_tree(t1, t2):
        return True
    return subtree_of(t1.left, t2) or subtree_of(t1.right, t2)

def match_tree(t1, t2):
    if t1 is None and t2 is None:
        return True
    if t1 is None:
        return False
    if t2 is None:
        return True
    if t1.val == t2.val:
        left = match_tree(t1.left, t2.left)
        right = match_tree(t1.right, t2.right)
        return (left and right)
    return False

def contains_tree_str(t1, t2):
    if t2 is None:
        return True
    return preorder_str(t1).index(preorder_str(t2)) > -1

def preorder_str(root):
    if root is None:
        return "L"
    return "{0},{1},{2}".format(root.val, preorder_str(root.left), preorder_str(root.right))

if __name__ == "__main__":
    t1 = TreeNode(0,
            TreeNode(1),
            None)
    t2 = TreeNode(2,
            TreeNode(3),
            TreeNode(4)
            )
    t1.right = t2
    print("Tree 1: {}\nTree 2: {}".format(t1, t2))
    print("Is tree 2 a subtree of tree 1? {}".format(contains_tree(t1, t2)))
    print("Is tree 2 a subtree of tree 1? (String pre-order method) {}".format(contains_tree_str(t1, t2)))

