
def merge(intervals: 'List[int]') -> 'List[int]':
    result = []
    for it in intervals:
        if len(result) == 0 or result[-1][1] < it[0]:
            result.append(it)
        else:
            result[-1][1] = max(result[-1][1], it[1])
    return result

if __name__ == "__main__":
    intervals = [[1, 3], [2, 4], [5, 7], [6, 8]]
    print("{} => {}".format(intervals, merge(intervals)))
