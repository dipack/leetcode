def merge_sort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2
        larr = arr[:mid]
        rarr = arr[mid:]

        merge_sort(larr)
        merge_sort(rarr)

        lidx, ridx, fidx = 0, 0, 0

        while lidx < len(larr) and ridx < len(rarr):
            if larr[lidx] < rarr[ridx]:
                arr[fidx] = larr[lidx]
                lidx += 1
            else:
                arr[fidx] = rarr[ridx]
                ridx += 1
            fidx += 1
        while lidx < len(larr):
            arr[fidx] = larr[lidx]
            lidx += 1
            fidx += 1
        while ridx < len(rarr):
            arr[fidx] = rarr[ridx]
            ridx += 1
            fidx += 1

if __name__ == "__main__":
    arr = [5, 6, 1, 7, 8, 0, 2]
    print(arr)
    merge_sort(arr)
    print(arr)
