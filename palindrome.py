import math

def palindrome(s):
    s = "".join(s.split(" "))
    if len(s) % 2 == 0:
        mid = len(s) // 2
        l, r = s[:mid], s[mid:]
    else:
        mid = int(math.floor(len(s) / 2))
        l, r = s[:mid], s[mid + 1:]
    return str(l) == str(r[::-1])

if __name__ == "__main__":
    s = "taco cat"
    print(s, palindrome(s))

    s = "atma"
    print(s, palindrome(s))

    s = "bongo ognob"
    print(s, palindrome(s))

