from structures import TreeNode

def bfs(graph, start, visited=None, debug=True):
    if visited is None:
        visited = set()
    queue = [start]
    while queue:
        node = queue.pop(0)
        if node not in visited:
            if debug:
                print("Visiting vertex {}".format(node))
            visited.add(node)
            queue.extend(graph[node] - visited)
    return visited

def bfs_tree(root, visited=None, debug=True):
    if visited is None:
        visited = set()
    if root is None:
        return visited
    queue = [root]
    while queue:
        node = queue.pop(0)
        if node not in visited and node is not None:
            if debug:
                print("Visiting node {}".format(node))
            visited.add(node)
            nnodes = set([node.left, node.right])
            queue.extend(nnodes - visited)
    return visited


if __name__ == "__main__":
    graph = {'a': set(['b', 'c']), 'b': set(['a', 'd', 'e']), 'c': set(['a', 'f']), 'd': set(['b']), 'e': set(['b', 'f']), 'f': set(['c', 'e'])}
    print(bfs(graph, 'c', None))
    tree = TreeNode(1,
            TreeNode(2,
                TreeNode(3),
                TreeNode(4,
                    None, TreeNode(5))),
                TreeNode(6,
                    TreeNode(7), None))
    print(bfs_tree(tree))

