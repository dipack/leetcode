def distance_covered(readings, end_time):
    if len(readings) == 0:
        return 0.0
    dist = 0.0
    idx = -1
    if end_time >= readings[idx][0]:
        idx = len(readings) - 1
    else:
        iidx = len(readings) - 1
        while iidx >= 0:
            t = readings[iidx][0]
            if end_time >= t:
                idx = iidx
                break
            iidx -= 1
    lt = readings[idx][0]
    ls = readings[idx][1]
    dist += abs(end_time - lt) * float(ls / 3600)
    idx -= 1
    while idx >= 0:
        newd = abs(lt - readings[idx][0]) * float(readings[idx][1] / 3600)
        dist += newd
        lt = readings[idx][0]
        idx -= 1
    return dist

if __name__ == "__main__":
    r = [[0, 90]]
    print(distance_covered(r, 100))
    r = [[0, 90], [20, 100]]
    print(distance_covered(r, 100))
    r = [[0, 60], [300, 50], [500, 45]]
    print(distance_covered(r, 700))
    r = [[0, 90], [200, 40], [300, 65]]
    print(distance_covered(r, 120))
    r = [[0, 60], [120, 55], [400, 95]]
    print(distance_covered(r, 368))
