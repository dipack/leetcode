from structures import TreeNode

def is_valid_bst(root, min_val=None, max_val=None):
    """
    Check if the BST is actually a true BST, i.e.,
    if _all_ left subtree elements are smaller than root,
    and _all_ right subtree elements are greater than root.
    """
    if root is None:
        return True

    if (min_val and root.val <= min_val) or (max_val and root.val >= max_val):
        return False

    if not is_valid_bst(root.left, min_val, root.val):
        return False

    if not is_valid_bst(root.right, root.val, max_val):
        return False

    return True

if __name__ == "__main__":
    tree = TreeNode(0,
            TreeNode(1,
                TreeNode(2),
                TreeNode(3,
                    TreeNode(4),
                    None)
                ),
            TreeNode(5,
                TreeNode(6,
                    TreeNode(7,
                        TreeNode(8),
                        None),
                    TreeNode(9)
                    ),
                None)
            )
    print(tree)
    print("Is valid BST? {}".format(is_valid_bst(tree)))
    tree = TreeNode(20,
            TreeNode(10,
                TreeNode(5),
                TreeNode(15)
                ),
            TreeNode(30,
                TreeNode(25),
                TreeNode(45)
                )
            )
    print(tree)
    print("Is valid BST? {}".format(is_valid_bst(tree)))

