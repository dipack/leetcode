def edit_distance(s1, s2, l1, l2):
    """
    Given two strings, find how many edit steps are they
    'away from each other.
    """
    if l1 == 0:
        return l2
    if l2 == 0:
        return l1
    if s1[l1 - 1] == s2[l2 - 1]:
        return edit_distance(s1, s2, l1 - 1, l2 - 1)
    return 1 + min(edit_distance(s1, s2, l1, l2 - 1), # Insert
            edit_distance(s1, s2, l1 - 1, l2), # Remove
            edit_distance(s1, s2, l1 - 1, l2 - 1) #Replace
            )

if __name__ == "__main__":
    s1, s2 = "hello", "hallo"
    print(s1, s2, edit_distance(s1, s2, len(s1), len(s2)))

    s1, s2 = "hello", "bye"
    print(s1, s2, edit_distance(s1, s2, len(s1), len(s2)))
