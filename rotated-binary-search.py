def rot_binary_search(nums, x):
    """
    Using binary search, find the element in a rotated sorted array.
    Eg: [4, 5, 6, 1, 2, 3], x = 2, answer = 4
    """
    l, r = 0, len(nums) - 1
    while l <= r:
        m = (l + r) // 2
        if nums[m] == x:
            return m
        # If at m, the array still hasn't rotated, then do this
        if nums[m] >= nums[l]:
            # If x is in the left-half of the array, before the mid
            if x >= nums[l] and x <= nums[m]:
                r = m - 1
            else:
                l = m + 1
        # If at m, the array HAS rotated
        else:
            # If x, is in the right-half of the array, after the mid
            if x >= nums[m] and x <= nums[r]:
                l = m + 1
            else:
                r = m - 1
    return -1

a = [4, 5, 6, 1, 2, 3]
x = 2
print(a, x, rot_binary_search(a, x))
a = [4, 5, 6, 7, 8]
x = 2
print(a, x, rot_binary_search(a, x))
a = [8, 7, 6, 5, 4, 3]
x = 4
print(a, x, rot_binary_search(a, x))
